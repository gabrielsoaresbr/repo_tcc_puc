package org.gabriel.tccpuc.services;

import javax.transaction.Transactional;

import org.gabriel.tccpuc.models.Insumo;
import org.gabriel.tccpuc.repository.InsumoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsumoService {
	
	@Autowired
	public InsumoRepository insumoRepository;
	
	@Transactional
	public Insumo findInsumoById(Long id) {
		return insumoRepository.findById(id).
				orElseThrow(() -> new RuntimeException("Insumo com ID: "+ id +" não encontrado"));
	}

}
