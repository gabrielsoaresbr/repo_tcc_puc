package org.gabriel.tccpuc.repository;

import org.gabriel.tccpuc.models.Insumo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsumoRepository extends JpaRepository<Insumo, Long> {
	Boolean existsByNome(String nome);
}