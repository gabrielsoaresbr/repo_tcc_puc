package org.gabriel.tccpuc.repository;

import java.util.Optional;

import org.gabriel.tccpuc.models.ERole;
import org.gabriel.tccpuc.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
