package org.gabriel.tccpuc.models;

public enum ERole {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_TECNICO,
    ROLE_GESTOR,
    ROLE_ADMIN
}