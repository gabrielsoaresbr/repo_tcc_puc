package org.gabriel.tccpuc.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(	name = "insumos")
public class Insumo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Campo Nome deve ser preenchido")
	@Size(max = 100, message = "Campo Nome não deve ultrapassar 100 caracteres")
	private String nome;

	@NotBlank(message = "Campo Nome do Fabricante deve ser preenchido")
	@Size(max = 100, message = "Campo Nome do Fabricante não deve ultrapassar 100 caracteres")
	private String nomeFabricante;
	
	@NotNull(message = "Campo Prazo deve ser preenchido")
	private Integer prazoProximaManutencaoEmMeses;
	
//	@JsonFormat(pattern="yyyy-MM-dd")
	@NotNull(message = "Campo Data da Última Manutenção deve ser preenchido")
	private Date dataUltimaManutencao;

	public Insumo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFabricante() {
		return nomeFabricante;
	}

	public void setNomeFabricante(String nomeFabricante) {
		this.nomeFabricante = nomeFabricante;
	}

	public Integer getPrazoProximaManutencaoEmMeses() {
		return prazoProximaManutencaoEmMeses;
	}

	public void setPrazoProximaManutencaoEmMeses(Integer prazoProximaManutencaoEmMeses) {
		this.prazoProximaManutencaoEmMeses = prazoProximaManutencaoEmMeses;
	}

	public Date getDataUltimaManutencao() {
		return dataUltimaManutencao;
	}

	public void setDataUltimaManutencao(Date dataUltimaManutencao) {
		this.dataUltimaManutencao = dataUltimaManutencao;
	}


}
