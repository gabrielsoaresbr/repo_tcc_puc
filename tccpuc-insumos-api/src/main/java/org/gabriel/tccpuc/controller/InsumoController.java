package org.gabriel.tccpuc.controller;

import java.util.List;

import javax.validation.Valid;

import org.gabriel.tccpuc.models.Insumo;
import org.gabriel.tccpuc.repository.InsumoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/insumo")
public class InsumoController {

	@Autowired
	private InsumoRepository insumoRepository;
	
	@GetMapping
	public List<Insumo> findAll() {
		return insumoRepository.findAll();
	}

	@GetMapping(value = "/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('TECNICO')")
	public ResponseEntity<Insumo> getInsumoById(@PathVariable Long id) {
		return insumoRepository.findById(id).map(insumo -> ResponseEntity.ok().body(insumo))
				.orElse(ResponseEntity.notFound().build());
	}

	@PutMapping(value = "/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('TECNICO')")
	public ResponseEntity<Insumo> update(@PathVariable("id") long id, @RequestBody @Valid Insumo insumo) {
		return insumoRepository.findById(id).map(record -> {
			record.setNome(insumo.getNome());
			record.setNomeFabricante(insumo.getNomeFabricante());
			record.setPrazoProximaManutencaoEmMeses(insumo.getPrazoProximaManutencaoEmMeses());
			record.setDataUltimaManutencao(insumo.getDataUltimaManutencao());
			Insumo updated = insumoRepository.save(record);
			return ResponseEntity.ok().body(updated);
		}).orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('TECNICO')")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		return insumoRepository.findById(id).map(record -> {
			insumoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('TECNICO')")
	public Insumo create(@RequestBody @Valid Insumo insumo) throws JsonProcessingException {
		Insumo i = insumoRepository.save(insumo);
		return i;
	}

}
