package org.gabriel.tccpuc.amqp;

import org.gabriel.tccpuc.models.Insumo;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class QueueSender {
	
	public static final String EXCHANGE_NAME = "tccpucExchange";
	
	@Autowired
    private RabbitTemplate rabbitTemplate;
 
    public void send(Insumo insumo) throws JsonProcessingException {
    	String json = new ObjectMapper().writeValueAsString(insumo);
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, "", insumo.getNome());
    }

}
