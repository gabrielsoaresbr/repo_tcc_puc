package org.gabriel.tccpucseguranca.amqp;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class QueueConsumer {
	
	public static final String BROKER = "ConsumerWebSocket";
	
	@Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
	
	@RabbitListener(queues = {"tccpucQueue"})
    public void consumer(Message message) {
        System.out.println("RECEBIDO: " + new String(message.getBody()));
        
        //Enviar pro front via Socket
        simpMessagingTemplate.convertAndSend(BROKER, new String(message.getBody()));
    }

}
