package org.gabriel.tccpucseguranca;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;

@SpringBootApplication
@EnableRabbit
@EnableWebSocket
@EnableWebSocketMessageBroker
public class TccpucSegurancaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TccpucSegurancaApplication.class, args);
	}

}
