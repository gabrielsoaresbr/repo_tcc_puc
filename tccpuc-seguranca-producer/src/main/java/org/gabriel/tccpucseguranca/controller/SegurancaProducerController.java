package org.gabriel.tccpucseguranca.controller;

import org.gabriel.tccpucseguranca.amqp.QueueSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/seguranca")
public class SegurancaProducerController {
	
	@Autowired
	private QueueSender queueSender;
	
	@PostMapping
	public void create(@RequestBody String msg) {
		System.out.println("MSG Recebida: " + msg);
		queueSender.send(msg);
		ResponseEntity.ok().build();
	}

}
