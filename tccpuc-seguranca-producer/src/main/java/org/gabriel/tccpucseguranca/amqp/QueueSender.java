package org.gabriel.tccpucseguranca.amqp;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QueueSender {
	
	public static final String EXCHANGE_NAME = "tccpucExchange";
	
	@Autowired
    private RabbitTemplate rabbitTemplate;
 
    public void send(String msg) {
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, "", msg);
    }

}
