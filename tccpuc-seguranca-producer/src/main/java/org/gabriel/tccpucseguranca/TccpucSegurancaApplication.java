package org.gabriel.tccpucseguranca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TccpucSegurancaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TccpucSegurancaApplication.class, args);
	}

}
