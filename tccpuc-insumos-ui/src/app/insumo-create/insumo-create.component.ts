import { BsModalRef } from 'ngx-bootstrap/modal';
import { InsumoService } from './../_services/insumo.service';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-insumo-create',
  templateUrl: './insumo-create.component.html',
  styleUrls: ['./insumo-create.component.scss']
})
export class InsumoCreateComponent implements OnInit {

  public addNewInsumoForm: FormGroup;
  public dataUltimaManutencao: Date;
  public event: EventEmitter<any>=new EventEmitter();

  constructor(
    private builder: FormBuilder, 
    private insumoService: InsumoService, 
    private bsModalRef: BsModalRef) {
  }

  ngOnInit():void {
    this.addNewInsumoForm = this.builder.group({
      nome: new FormControl('', [Validators.required]),
      nomeFabricante: new FormControl('', [Validators.required]),
      prazoProximaManutencaoEmMeses: new FormControl('', [Validators.required]),
      dataUltimaManutencao: new FormControl(new Date(), [Validators.required])
    });
  }

  public onInsumoFormSubmit(): void {
    let insumoData = {
      'nome': this.addNewInsumoForm.get('nome').value,
      'nomeFabricante': this.addNewInsumoForm.get('nomeFabricante').value,
      'prazoProximaManutencaoEmMeses': this.addNewInsumoForm.get('prazoProximaManutencaoEmMeses').value,
      'dataUltimaManutencao': this.addNewInsumoForm.get('dataUltimaManutencao').value
    };
  
    this.insumoService.create(insumoData).subscribe(
      data => {
        console.log(data);
        if (data != null && data.id > 0) {
          this.event.emit('OK');
          this.bsModalRef.hide();
        }
      }, 
      error => {
        console.log(error);
        if (error.status == 403) {
          this.bsModalRef.hide();
        }
      });
  }

  public onClose(): void {
    this.bsModalRef.hide();
  }

  get nome() {
    return this.addNewInsumoForm.get('nome');
  }

  get nomeFabricante() {
    return this.addNewInsumoForm.get('nomeFabricante');
  }

  get prazoProximaManutencaoEmMeses() {
    return this.addNewInsumoForm.get('prazoProximaManutencaoEmMeses');
  }

}
