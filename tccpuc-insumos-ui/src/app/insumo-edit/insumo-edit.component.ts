import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { InsumoService } from './../_services/insumo.service';

@Component({
  selector: 'app-insumo-edit',
  templateUrl: './insumo-edit.component.html',
  styleUrls: ['./insumo-edit.component.scss']
})
export class InsumoEditComponent implements OnInit {

  public editInsumoForm: FormGroup;
  public insumoId: number;
  public insumoData: any;
  public event: EventEmitter<any> = new EventEmitter();

  constructor(private builder: FormBuilder, private insumoService: InsumoService, private bsModalRef: BsModalRef) {
    this.editInsumoForm = this.builder.group({
      nome: new FormControl('', [Validators.required]),
      nomeFabricante: new FormControl('', []),
      prazoProximaManutencaoEmMeses: new FormControl('', []),
      dataUltimaManutencao: new FormControl(new Date(), [])
    });

    this.insumoService.insumoIdData.subscribe(data => {
      this.insumoId = data;
      if (this.insumoId !== undefined) {
        this.insumoService.read(this.insumoId).subscribe(data => {
          this.insumoData = data;
          
          if (this.editInsumoForm!=null && this.insumoData!=null) {
            this.editInsumoForm.controls['nome'].setValue(this.insumoData.nome);
            this.editInsumoForm.controls['nomeFabricante'].setValue(this.insumoData.nomeFabricante);
            this.editInsumoForm.controls['prazoProximaManutencaoEmMeses'].setValue(this.insumoData.prazoProximaManutencaoEmMeses);
            this.editInsumoForm.controls['dataUltimaManutencao'].setValue(new Date(this.insumoData.dataUltimaManutencao));
          }
        }, error => { console.log("Error while gettig insumo details") });
      }
    });
  }

  public onInsumoEditFormSubmit(): void {
    let insumoData = {
      'id': this.insumoId,
      'nome': this.editInsumoForm.get('nome').value,
      'nomeFabricante': this.editInsumoForm.get('nomeFabricante').value,
      'prazoProximaManutencaoEmMeses': this.editInsumoForm.get('prazoProximaManutencaoEmMeses').value,
      'dataUltimaManutencao': this.editInsumoForm.get('dataUltimaManutencao').value,
    };

    this.insumoService.update(this.insumoId, insumoData).subscribe(data => {      
        this.event.emit('OK');
        this.bsModalRef.hide();      
    });
  }

  public onClose(): void {
    this.bsModalRef.hide();
  }

  ngOnInit() {
  }

}
