import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router) { }

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        // this.tokenStorageService?.signOut;
        window.sessionStorage.clear();
        // window.location.reload();
        this.router.navigateByUrl(`/login`);
      }

      //handle your auth error or rethrow
      if (err.status === 403) {
          //navigate /delete cookies or whatever
          this.router.navigateByUrl(`/forbidden`);
      }
      return throwError(err);
  }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => this.handleAuthError(err)
            /*{if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.tokenStorageService.signOut;
                window.location.reload();
            }

            if (err.status === 403) {
              // if 403 show msg forbbiden
              this.router.navigate(['/forbidden']);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);}*/
        ))
    }
}

export const errorInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, // useClass: ErrorInterceptor, 
    useFactory: function(router: Router) {
      return new ErrorInterceptor(router);
    },
    multi: true,
    deps: [Router] 
  }
];