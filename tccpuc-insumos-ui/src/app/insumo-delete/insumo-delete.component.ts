import { InsumoService } from './../_services/insumo.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-insumo-delete',
  templateUrl: './insumo-delete.component.html',
  styleUrls: ['./insumo-delete.component.scss']
})
export class InsumoDeleteComponent implements OnInit {

  public insumoId: number;
  public nome: string;
  public event: EventEmitter<any> = new EventEmitter();
  
  constructor(private bsModalRef: BsModalRef, private insumoService: InsumoService) {
  }

  public deleteInsumo(insumoId: number): void {
    this.insumoService.delete(insumoId).subscribe(
      () => {
        this.event.emit('OK');
        this.bsModalRef.hide();
      }, error => {
        console.log("Error while delete insumo ", error);
        this.bsModalRef.hide();
      }
    );
  }

  public onClose(): void {
    this.bsModalRef.hide();
  }

  ngOnInit() {
  }

}
