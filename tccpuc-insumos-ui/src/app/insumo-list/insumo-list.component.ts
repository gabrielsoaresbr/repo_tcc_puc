import { TokenStorageService } from './../_services/token-storage.service';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { InsumoCreateComponent } from './../insumo-create/insumo-create.component';
import { InsumoDeleteComponent } from './../insumo-delete/insumo-delete.component';
import { InsumoEditComponent } from './../insumo-edit/insumo-edit.component';
import { InsumoService } from './../_services/insumo.service';

@Component({
  selector: 'app-insumo-list',
  templateUrl: './insumo-list.component.html',
  styleUrls: ['./insumo-list.component.scss']
})
export class InsumoListComponent implements OnInit {

  public loading = false;
  public insumoList: any[] = [];
  public bsModalRef: BsModalRef;
  public usuarioLogado: any;
  public isAdmin: boolean;
  public isUser: boolean;
  public isMod: boolean;
  public isTec: boolean;
  public isGestor: boolean;

  constructor(
    private insumoService: InsumoService,
    private bsModalService: BsModalService,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.getInsumos();
    this.getRolesUsuarioLogado();
  }

  private getRolesUsuarioLogado(): void {
    this.usuarioLogado = this.tokenStorageService.getUser();
    this.usuarioLogado.roles.forEach(role => {
      this.isAdmin = role == 'ROLE_ADMIN';
    });
    this.usuarioLogado.roles.forEach(role => {
      this.isUser = role == 'ROLE_USER';
    });
    this.usuarioLogado.roles.forEach(role => {
      this.isMod = role == 'ROLE_MODERATOR';
    });
    this.usuarioLogado.roles.forEach(role => {
      this.isTec = role == 'ROLE_TECNICO';
    });
    this.usuarioLogado.roles.forEach(role => {
      this.isGestor = role == 'ROLE_GESTOR';
    });
  }

  public getInsumos(): void {
    this.loading = true;
    this.insumoService.readAll().subscribe(data => {
      Object.assign(this.insumoList, data);
      this.loading = false;
    }, error => {
      console.log("Error while getting insumos ", error);
      this.loading = false;
    });
  }

  public addNewInsumo(): void {
    this.bsModalRef = this.bsModalService.show(InsumoCreateComponent, {class: 'modal-lg', backdrop: 'static'});
    this.bsModalRef.content.event.subscribe(result => {
      if (result == 'OK') {
        this.getInsumos();
      }
    });
  }

  public deleteInsumo(id: number, nome: string): void {
    this.bsModalRef = this.bsModalService.show(InsumoDeleteComponent, {class: 'modal-lg', backdrop: 'static'});
    this.bsModalRef.content.insumoId = id;
    this.bsModalRef.content.nome = nome;
    this.bsModalRef.content.event.subscribe(result => {
      console.log("deleted", result);
      if (result == 'OK') {
        this.insumoList=[];
        this.getInsumos();
      }
    });
  }

  public editInsumo(insumoId: number): void {
    this.insumoService.changeInsumoId(insumoId);
    this.bsModalRef = this.bsModalService.show(InsumoEditComponent, {class: 'modal-lg', backdrop: 'static'});
    this.bsModalRef.content.event.subscribe(result => {
      if (result == 'OK') {
        this.getInsumos();
      }
    });
  }

}
