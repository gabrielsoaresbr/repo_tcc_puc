import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay } from 'rxjs/operators';
import { environment } from "../../environments/environment";

// const API_URL = 'http://localhost:8080/api/insumo';

@Injectable({
  providedIn: 'root'
})
export class InsumoService {

  insumoIdSource = new  BehaviorSubject<number>(0);
  insumoIdData: any;

  baseUrl = environment.baseUrl + 'api/insumo';

  constructor(private httpClient: HttpClient) { 
    this.insumoIdData = this.insumoIdSource.asObservable();
  }

  readAll(): Observable<any> {
    return this.httpClient.get(this.baseUrl).pipe(delay(2000));;
  }

  read(id): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}/${id}`);
  }

  create(data): Observable<any> {
    return this.httpClient.post(this.baseUrl, data);
  }

  update(id, data): Observable<any> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }

  changeInsumoId(insumoId: number){
    this.insumoIdSource.next(insumoId);
  }
}
