import { TokenStorageService } from './../_services/token-storage.service';
import { Component, OnInit } from '@angular/core';
import { GraficoDTO } from './../_models/grafico-dto';
import { Insumo } from './../_models/insumo';
import { InsumoService } from './../_services/insumo.service';
import { dados } from './data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content?: string;
  single: any[];
  view: any[] = [400, 200];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  usuarioLogado: any;
  isGestor: boolean;

  dadosGrafico: GraficoDTO[] = [];

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private insumoService: InsumoService,
    private tokenStorageService: TokenStorageService) {
    Object.assign(this, { dados });
    
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  ngOnInit(): void {
    this.getRolesUsuarioLogado();

    if (this.isGestor) {
      this.insumoService.readAll().subscribe(
        (insumos: Insumo[]) => {
          var countVencidos = 0;
          var countEmDia = 0;
          insumos.forEach(i => {
            let dataUltimaManut = this.formatarData(i.dataUltimaManutencao);
            const dataProximaManut: Date = new Date(dataUltimaManut.setMonth(dataUltimaManut.getMonth() + i.prazoProximaManutencaoEmMeses));
            if (new Date() > dataProximaManut) {
              countVencidos++;
            } else {
              countEmDia++;
            }
          });
          this.dadosGrafico.push(new GraficoDTO('Vencidos', countVencidos));
          this.dadosGrafico.push(new GraficoDTO('Em dia', countEmDia));
          this.single = this.dadosGrafico;
        }
      );
    }
  }

  private getRolesUsuarioLogado(): void {
    this.usuarioLogado = this.tokenStorageService.getUser();
    if (this.usuarioLogado && this.usuarioLogado.roles){
      this.usuarioLogado.roles.forEach(role => {
        this.isGestor = role == 'ROLE_GESTOR';
      });
    }
  }

  formatarData(data: any): Date {
    let options: Intl.DateTimeFormatOptions = {
      hour: "2-digit", minute: "2-digit", second: "2-digit"
    };
    const dataString = new Date(data).toLocaleDateString();
    return new Date(`${dataString.substr(6, 4)} ${dataString.substr(3, 2)} ${dataString.substr(0, 2)} ${dataString.substr(11, 13)}`);
  }
}
