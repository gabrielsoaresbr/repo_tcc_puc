export class GraficoDTO {
  public name?: any;
  public value?: any;

  constructor(name?: any, value?: any) {
    this.name = name;
    this.value = value;
  }
}