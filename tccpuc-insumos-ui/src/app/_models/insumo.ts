export class Insumo {
  public id: number;
	public nome: string;
	public nomeFabricante: string;
	public prazoProximaManutencaoEmMeses: number;
	public dataUltimaManutencao: Date;
}