import { WebSocketConnector } from './web-socket-connector';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title: string = 'Módulo Segurança!'
  items: any[] = [];
  private webSocketConnector: WebSocketConnector;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.webSocketConnector = new WebSocketConnector(
      'http://localhost:9000/socket',
      'ConsumerWebSocket',
      this.onMessage.bind(this)
    );
  }

  onMessage(message: any): void {
    this.items.push(message.body);
  }
}